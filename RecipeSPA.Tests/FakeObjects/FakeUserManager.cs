﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using RecipeSPA.Models;
using System;

namespace RecipeSPA.Tests.FakeObjects
{
    public class FakeUserManager<T> : UserManager<T> where T: IdentityUser
    {
        public FakeUserManager()
            : base(new Mock<IUserStore<T>>().Object,
                  new Mock<IOptions<IdentityOptions>>().Object,
                  new Mock<IPasswordHasher<T>>().Object,
                  new IUserValidator<T>[0],
                  new IPasswordValidator<T>[0],
                  new Mock<ILookupNormalizer>().Object,
                  new Mock<IdentityErrorDescriber>().Object,
                  new Mock<IServiceProvider>().Object,
                  new Mock<ILogger<UserManager<T>>>().Object)
        { }

    }
}

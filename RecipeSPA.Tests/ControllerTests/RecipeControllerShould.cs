﻿using RecipeSPA.Controllers.API;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using RecipeSPA.Interfaces;
using Microsoft.Extensions.Logging;
using RecipeSPA.Models;
using Xunit;
using RecipeSPA.Tests.FakeObjects;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using RecipeSPA.Models.ViewModels;
using AutoMapper;

namespace RecipeSPA.Tests.ControllerTests
{    
    public class RecipeControllerShould
    {
        private RecipeController _sut { get; set; }
        private Mock<IUnitOfWork> _mockUoW { get; set; }
        private Mock<ILogger<RecipeController>> _mockLogger { get; set; }
        private Mock<FakeUserManager<RecipeUser>> _mockUserManager { get; set; }
        private RecipeUser RecipeUser
        {
            get
            {
                if (_recipeUser == null)
                    _recipeUser = new RecipeUser { Email = "testUser@mail.com" };

                return _recipeUser;
            }
        }
        private RecipeUser _recipeUser;

        public RecipeControllerShould()
        {
            _mockUoW = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger<RecipeController>>();
            _mockUserManager = new Mock<FakeUserManager<RecipeUser>>();

            _sut = new RecipeController(_mockUoW.Object, _mockLogger.Object, _mockUserManager.Object);

            var _mockHttpContext = new Mock<HttpContext>();

            _mockHttpContext.SetupGet(ctx => ctx.User).Returns(new GenericPrincipal(new GenericIdentity("email@fake.com"), null));
            _mockUserManager.Setup(um => um.GetUserAsync(It.IsAny<ClaimsPrincipal>())).Returns(Task.FromResult<RecipeUser>(RecipeUser));

            _sut.ControllerContext.HttpContext = _mockHttpContext.Object;

            Mapper.Initialize(config =>
            {
                config.CreateMap<RecipeViewModel, Recipe>().ReverseMap();
            });
        }

        [Fact]
        public void ExceptionInGetShouldReturn500StatusCode()
        {
            _mockUoW.Setup(uow => uow.GetAllRecipesByUser(RecipeUser)).Throws(new NullReferenceException());

            Assert.Equal(500,((ObjectResult)_sut.Read().Result).StatusCode);
        }

        [Fact]
        public void SuccessfulGetShouldReturn200StatusCode()
        {
            _mockUoW.Setup(uow => uow.GetAllRecipesByUser(RecipeUser)).Returns(Task.FromResult((IEnumerable<Recipe>)new List<Recipe>()));

            Assert.Equal(200, ((ObjectResult)_sut.Read().Result).StatusCode);
        }

        [Fact]
        public void GetShouldReturnListOfRecipes()
        {
            _mockUoW.Setup(uow => uow.GetAllRecipesByUser(RecipeUser)).Returns(Task.FromResult((IEnumerable<Recipe>)new List<Recipe>()));

            Assert.IsType<List<Recipe>>(((ObjectResult)_sut.Read().Result).Value);
        }

        [Fact]
        public void InvalidModelStateInCreateShouldReturn400StatusCode()
        {
            _sut.ModelState.AddModelError("Recipe Model Error", "Invalid Model");

            Assert.Equal(400,((ObjectResult)_sut.Create(It.IsAny<RecipeViewModel>()).Result).StatusCode);
        }

        [Fact]
        public void CreateShouldReturn201StatusCode()
        {
            _mockUoW.Setup(uow => uow.AddRecipe(It.IsAny<Recipe>())).Returns(new Recipe());
            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));
            var rvm = new RecipeViewModel { Name = "Recipe", ServingSize = 1, Ingredients = new List<Ingredient>() };

            Assert.Equal(201,((ObjectResult)_sut.Create(rvm).Result).StatusCode);
        }

        [Fact]
        public void CreateShouldReturnRecipeObjectInResponse()
        {
            _mockUoW.Setup(uow => uow.AddRecipe(It.IsAny<Recipe>())).Returns(new Recipe());
            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            var rvm = new RecipeViewModel { Name = "Recipe", ServingSize = 1, Ingredients = new List<Ingredient>() };

            Assert.IsType<Recipe>(((ObjectResult)_sut.Create(rvm).Result).Value);
        }

        [Fact]
        public void CreateShouldReturnRecipeWithSameValuesAsRecipeViewModelArgumentPassed()
        {
            var rvm = new RecipeViewModel { Name = "Recipe", ServingSize = 1, Ingredients = new List<Ingredient> { Mock.Of<Ingredient>(), Mock.Of<Ingredient>() } };

            _mockUoW.Setup(uow => uow.AddRecipe(It.IsAny<Recipe>())).Returns(Mapper.Map<Recipe>(rvm));
            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            var result = (Recipe)((ObjectResult)_sut.Create(rvm).Result).Value;

            Assert.True(result.Name == rvm.Name && result.ServingSize == rvm.ServingSize && rvm.Ingredients.Count == rvm.Ingredients.Count);
        }

        [Fact]
        public void DuplicateRecipeNameExceptionShouldBeCaughtAndReturned()
        {
            string exMsg = "Duplicate name message";

            var rvm = new RecipeViewModel { Name = "Recipe", ServingSize = 1, Ingredients = new List<Ingredient> { Mock.Of<Ingredient>(), Mock.Of<Ingredient>() } };

            _mockUoW.Setup(uow => uow.AddRecipe(It.IsAny<Recipe>())).Throws(new DuplicateRecipeNameException(exMsg));

            Assert.Equal(exMsg,((ObjectResult)_sut.Create(rvm).Result).Value);
        }

        [Fact]
        public void DeleteShouldReturn200StatusCode()
        {
            _mockUoW.Setup(uow => uow.DeleteRecipe(It.IsAny<int>())).Callback(() => { });

            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            Assert.Equal(200, ((ObjectResult)_sut.Delete(1).Result).StatusCode);
        }

        [Fact]
        public void ErrorInDeletingShouldReturnExceptionMessage()
        {
            string exMsg = "Exception message returned";

            _mockUoW.Setup(uow => uow.DeleteRecipe(It.IsAny<int>())).Throws(new Exception(exMsg));

            Assert.Equal(exMsg, ((ObjectResult)_sut.Delete(1).Result).Value);
        }

        [Fact]
        public void UpdateWithPatchedNameShouldReturnRecipeWithSameValue()
        {
            RecipePatchModel recipe = new RecipePatchModel { Name = "Updated Recipe" };

            _mockUoW.Setup(uow => uow.UpdateRecipe(recipe, It.IsAny<int>())).Returns(new Recipe { Name = recipe.Name });

            Assert.Equal(recipe.Name, ((Recipe)((ObjectResult)_sut.Update(recipe, 1).Result).Value).Name);
        }

        [Fact]
        public void UpdateWithPatchedServSizeeShouldReturnRecipeWithSameValue()
        {
            RecipePatchModel recipe = new RecipePatchModel { ServingSize = 5};

            _mockUoW.Setup(uow => uow.UpdateRecipe(recipe, It.IsAny<int>())).Returns(new Recipe { ServingSize = recipe.ServingSize ?? 1 });

            Assert.Equal(recipe.ServingSize, ((Recipe)((ObjectResult)_sut.Update(recipe, 1).Result).Value).ServingSize);
        }
    }
}

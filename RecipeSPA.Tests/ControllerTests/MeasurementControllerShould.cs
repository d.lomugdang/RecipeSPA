﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using RecipeSPA.Controllers.API;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RecipeSPA.Tests.ControllerTests
{
    public class MeasurementControllerShould
    {
        public MeasurementController _sut { get; set; }
        public Mock<IUnitOfWork> _mockUoW { get; set; }

        public MeasurementControllerShould()
        {
            _mockUoW = new Mock<IUnitOfWork>();
            _sut = new MeasurementController(_mockUoW.Object, new Mock<ILogger<MeasurementController>>().Object);            
        }

        [Fact]
        public void SuccessfulGetShouldReturn200()
        {
            _mockUoW.Setup(uow => uow.GetAllMeasurements()).Returns(Task.FromResult(It.IsAny<IEnumerable<Measurement>>()));

            Assert.Equal(200, ((ObjectResult)_sut.Get().Result).StatusCode);
        }
        
    }
}

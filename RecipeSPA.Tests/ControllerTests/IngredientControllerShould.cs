﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using RecipeSPA.Controllers.API;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Models.ViewModels;
using System;
using System.Threading.Tasks;
using Xunit;

namespace RecipeSPA.Tests.ControllerTests
{
    public class IngredientControllerShould
    {
        private Mock<IUnitOfWork> _mockUoW { get; set; }
        private IngredientController _sut { get; set; }
        private Mock<ILogger<IngredientController>> _mockLogger { get; set; }

        public IngredientControllerShould()
        {
            _mockUoW = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger<IngredientController>>();
            _sut = new IngredientController(_mockUoW.Object, _mockLogger.Object);
        }

        [Fact]
        public void ErrorsWhileGettingRecipeNamesWillStillReturnEmptyString()
        {
            _mockUoW.Setup(uow => uow.GetAllIngredients()).Throws(It.IsAny<Exception>());

            Assert.Equal("", ((ObjectResult)_sut.Get().Result).Value);
        }


        [Fact]
        public void ErrorsWhileGettingRecipeNamesWillReturn200StatusCode()
        {
            _mockUoW.Setup(uow => uow.GetAllIngredients()).Throws(It.IsAny<Exception>());

            Assert.Equal(200, ((ObjectResult)_sut.Get().Result).StatusCode);
        }

        [Fact]
        public void CreatedIngredientMustHaveSameNameValueFromIngredientModel()
        {
            IngredientViewModel vm = new IngredientViewModel { Name = "New Ingredient", Amount = 5, Measurement = new Measurement(), RecipeId = 1 };
            _mockUoW.Setup(uow => uow.AddIngredient(vm)).Returns(new Ingredient { Name = vm.Name });

            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            Assert.Equal(vm.Name, ((Ingredient)((ObjectResult)_sut.Post(vm).Result).Value).Name);
        }

        [Fact]
        public void CreatedIngredientMustHaveSameAmountValueFromIngredientModel()
        {
            IngredientViewModel vm = new IngredientViewModel { Name = "New Ingredient", Amount = 37, Measurement = new Measurement(), RecipeId = 1 };
            _mockUoW.Setup(uow => uow.AddIngredient(vm)).Returns(new Ingredient { Amount = vm.Amount });

            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            Assert.Equal(vm.Amount, ((Ingredient)((ObjectResult)_sut.Post(vm).Result).Value).Amount );
        }

        [Fact]
        public void CreatedIngredientMustHaveSameMeasurementValueFromIngredientModel()
        {
            IngredientViewModel vm = new IngredientViewModel { Name = "New Ingredient", Amount = 37, Measurement = new Measurement(), RecipeId = 1 };
            _mockUoW.Setup(uow => uow.AddIngredient(vm)).Returns(new Ingredient { Measurement = vm.Measurement });

            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            Assert.Same(vm.Measurement, ((Ingredient)((ObjectResult)_sut.Post(vm).Result).Value).Measurement);
        }

        [Fact]
        public void ShouldReturn404WhenRecipeIdIsNotFound()
        {
            IngredientViewModel vm = new IngredientViewModel { Name = "New Ingredient", Amount = 1, Measurement = new Measurement(), RecipeId = 1 };
            _mockUoW.Setup(uow => uow.AddIngredient(vm)).Throws(new DBReferenceNotFoundException("", typeof(Recipe)));

            Assert.Equal(404, ((ObjectResult)_sut.Post(vm).Result).StatusCode);
        }

        [Fact]
        public void ShouldReturn200WhenDeleteIsSuccessful()
        {
            _mockUoW.Setup(uow => uow.DeleteIngredient(It.IsAny<int>(), It.IsAny<int>())).Callback(() => { });
            _mockUoW.Setup(uow => uow.SaveChangesAsync()).Returns(Task.FromResult(true));

            Assert.Equal(200, ((ObjectResult)_sut.Delete(It.IsAny<int>(), It.IsAny<int>()).Result).StatusCode);
        }

        [Fact]
        public void ShouldReturn404WhenDeleteIsUnsuccessfulDueToNullDBReference()
        {
            _mockUoW.Setup(uow => uow.DeleteIngredient(It.IsAny<int>(), It.IsAny<int>())).Throws(new DBReferenceNotFoundException("Test", typeof(Recipe)));

            Assert.Equal(404, ((ObjectResult)_sut.Delete(It.IsAny<int>(), It.IsAny<int>()).Result).StatusCode);
        }
    }
}

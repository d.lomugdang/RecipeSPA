﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using RecipeSPA.Controllers.API;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Tests.FakeObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Tests.ControllerTests
{
    public class ScheduledRecipeControllerShould
    {
        private Mock<IUnitOfWork> _mockUoT;
        private Mock<FakeUserManager<RecipeUser>> _mockUserManager;
        private RecipeUser RecipeUser
        {
            get
            {
                if (_recipeUser == null)
                    _recipeUser = new RecipeUser { Email = "testUser@mail.com" };

                return _recipeUser;
            }
        }
        private RecipeUser _recipeUser;

        public ScheduledRecipeControllerShould()
        {
            _mockUoT = new Mock<IUnitOfWork>();
            _mockUserManager = new Mock<FakeUserManager<RecipeUser>>();
            ScheduledRecipeController _sut = new ScheduledRecipeController(_mockUoT.Object, 
                                                    new Mock<ILogger<ScheduledRecipeController>>().Object,
                                                    _mockUserManager.Object);

            var _mockHttpContext = new Mock<HttpContext>();

            _mockHttpContext.SetupGet(ctx => ctx.User).Returns(new GenericPrincipal(new GenericIdentity("email@fake.com"), null));
            _mockUserManager.Setup(um => um.GetUserAsync(It.IsAny<ClaimsPrincipal>())).Returns(Task.FromResult<RecipeUser>(RecipeUser));

            _sut.ControllerContext.HttpContext = _mockHttpContext.Object;
        }


    }
}

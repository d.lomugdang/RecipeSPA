﻿using RecipeSPA.Models;
using RecipeSPA.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace RecipeSPA.Tests.ModelTests
{
    public class RecipeViewModelShould
    {
        [Theory]
        [InlineData("1")]
        [InlineData("22")]
        [InlineData("333")]
        [InlineData("4444")]
        public void NameWithLessThanFiveLettersShouldBeInvalid(string shortRecipeName)
        {
            var rvm = new RecipeViewModel { Name = shortRecipeName, ServingSize = 1, Ingredients = new List<Ingredient>() };
            var ctx = new ValidationContext(rvm);
            ICollection<ValidationResult> coll = new List<ValidationResult>();

            bool invalid = Validator.TryValidateObject(rvm, ctx, coll, true);

            Assert.False(invalid);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(0.5)]
        [InlineData(0.99999)]
        [InlineData(-1)]
        public void ServingSizeShouldBeAtLeast1(double servSize)
        {
            var rvm = new RecipeViewModel { Name = "Recipe", ServingSize = servSize, Ingredients = new List<Ingredient>() };
            var ctx = new ValidationContext(rvm);
            ICollection<ValidationResult> coll = new List<ValidationResult>();

            bool invalid = Validator.TryValidateObject(rvm, ctx, coll, true);

            Assert.False(invalid);
        }
    }
}

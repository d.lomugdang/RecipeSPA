﻿
using RecipeSPA.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace RecipeSPA.Tests.ModelTests
{
    public class RecipePatchModelShould
    {
        [Theory]
        [InlineData("1")]
        [InlineData("22")]
        [InlineData("333")]
        [InlineData("4444")]
        public void NameWithLessThanFiveLettersShouldBeInvalid(string shortRecipeName)
        {
            var rvm = new RecipePatchModel { Name = shortRecipeName, ServingSize = 1 };
            var ctx = new ValidationContext(rvm);
            ICollection<ValidationResult> coll = new List<ValidationResult>();

            bool invalid = Validator.TryValidateObject(rvm, ctx, coll, true);

            Assert.False(invalid);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ServingSizeShouldBeAtLeast1(int servSize)
        {
            var rvm = new RecipePatchModel { Name = "Recipe", ServingSize = servSize };
            var ctx = new ValidationContext(rvm);
            ICollection<ValidationResult> coll = new List<ValidationResult>();

            bool invalid = Validator.TryValidateObject(rvm, ctx, coll, true);

            Assert.False(invalid);
        }
    }
}

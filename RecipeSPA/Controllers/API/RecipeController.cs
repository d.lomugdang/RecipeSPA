﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RecipeSPA.DAL;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Controllers.API
{
    [Route("api/recipes")]
    [Authorize()]
    public class RecipeController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private ILogger<RecipeController> _logger;
        private UserManager<RecipeUser> _userManager;

        public RecipeController(IUnitOfWork unitOfWork, ILogger<RecipeController> logger, UserManager<RecipeUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Read()
        {
            try
            {
                _logger.LogInformation("Getting all recipes..");

                var user = _userManager.GetUserAsync(User).Result;

                var results = await _unitOfWork.GetAllRecipesByUser(user);                

                return Ok(results);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get recipes: {ex}");

                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }            
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]RecipeViewModel recipe)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    _logger.LogInformation("Adding new recipe..");

                    Recipe newRecipe = Mapper.Map<Recipe>(recipe);

                    newRecipe.User = await _userManager.GetUserAsync(User);

                    newRecipe = _unitOfWork.AddRecipe(newRecipe);

                    var res = _unitOfWork.SaveChangesAsync().Result;

                    return Created($"api/recipes/{newRecipe.Id}", newRecipe);
                }
                catch(DuplicateRecipeNameException ex)
                {
                    _logger.LogError(ex.Message);

                    ModelState.AddModelError("Duplicate Name", ex.Message);

                    return BadRequest(ex.Message);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Failed to add recipe: {ex}");

                    return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
                }
            }

            return BadRequest($"Please verify request body. Model error: {ModelState}");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if(id != 0)
            {
                try
                {
                    _logger.LogInformation("Deleting recipe..");

                    //_repository.DeleteRecipeById(id,this.User.Identity.Name);
                    _unitOfWork.DeleteRecipe(id);

                    if (await _unitOfWork.SaveChangesAsync())
                    {
                        return Ok("Recipe deleted");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Failed to delete recipe: {ex}");
                    return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
                }
            }

            return BadRequest("Invalid id in request.");
        }

        [HttpPatch("{id}")]        
        public async Task<IActionResult> Update([FromBody]RecipePatchModel recipe,int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _logger.LogInformation("Updating recipe..");

                    var updated = _unitOfWork.UpdateRecipe(recipe, id);

                    await _unitOfWork.SaveChangesAsync();

                    return Ok(updated);
                }
                catch (DBReferenceNotFoundException ex)
                {
                    _logger.LogError($"Update failed. Changes are rolled back with error: {ex}");
                    return NotFound($"Unable to update: {ex.Message}.");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Update failed. Changes are rolled back with error: {ex}");
                    return BadRequest($"Server error: {ex.Message}");
                }
            }

            return BadRequest(ModelState);
        }
    }
}

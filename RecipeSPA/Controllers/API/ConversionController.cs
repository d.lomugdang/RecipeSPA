﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RecipeSPA.DAL;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Controllers.API
{
    [Route("api/conversion")]
    [Authorize]
    public class ConversionController : Controller
    {
        private ILogger<ConversionController> _logger;
        private IUnitOfWork _unitOfWork;

        public ConversionController(IUnitOfWork unitOfWork, ILogger<ConversionController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        [HttpGet("config")]
        public async Task<IActionResult> Get()
        {
            try
            {
                _logger.LogInformation("Retrieving conversions configuration");

                var result = await _unitOfWork.GetAllConversions();

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError("Unable to retrieve config file");
                return BadRequest(ex.Message);
            }
        }
    }
}

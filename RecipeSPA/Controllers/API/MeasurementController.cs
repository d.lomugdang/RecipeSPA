﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RecipeSPA.DAL;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Controllers.API
{
    [Route("/api/measurements")]
    [Authorize()]
    public class MeasurementController : Controller
    {
        private ILogger<MeasurementController> _logger;
        private IUnitOfWork _unitOfWork;

        public MeasurementController(IUnitOfWork unitOfWork, ILogger<MeasurementController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var measurements = await _unitOfWork.GetAllMeasurements();

                return Ok(measurements);
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get measurements: {0}", ex.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        
    }
}

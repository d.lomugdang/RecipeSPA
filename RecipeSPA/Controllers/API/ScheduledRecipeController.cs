﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using RecipeSPA.DAL;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Controllers.API
{
    [Route("api/recipesched")]
    [Authorize]
    public class ScheduledRecipeController : Controller
    {
        private ILogger<ScheduledRecipeController> _logger;
        private IUnitOfWork _unitOfWork;
        private UserManager<RecipeUser> _userManager;

        public ScheduledRecipeController(IUnitOfWork unitOfWork,ILogger<ScheduledRecipeController> logger,UserManager<RecipeUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]ScheduledRecipeViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _logger.LogInformation("Retrieving recipe...");

                    //var recipe = _repository.GetRecipeById(vm.RecipeId);

                    var recipe = _unitOfWork.GetRecipeById(vm.RecipeId);

                    if (recipe == null)
                        return NotFound($"Unable to retrieve recipe with id: {vm.RecipeId}");

                    //var schedRecipe = _repository.AddScheduledRecipe(recipe, vm.Date.ToLocalTime());

                    var schedRecipe = _unitOfWork.AddScheduledRecipe(new ScheduledRecipe() { Recipe = recipe, Date = vm.Date.ToLocalTime() });

                    var result = await _unitOfWork.SaveChangesAsync();

                    if (result)
                        return Ok(schedRecipe.Id);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            _logger.LogError("Bad model request");
            return BadRequest("Please try again.");
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _logger.LogInformation("Retrieving scheduled recipes...");

                    var recipes = (await _unitOfWork.GetAllRecipesByUser(_userManager.GetUserAsync(User).Result)).Select(item => item.Id).Distinct();

                    var scheduledRecipes = await _unitOfWork.GetScheduledRecipes(recipes);

                    return Ok(scheduledRecipes);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            _logger.LogError("Unable to retrieve scheduled recipes");
            return BadRequest("Unable to retrieve scheduled recipes");
        }

        [HttpGet("shoppinglist")]
        public async Task<IActionResult> GetShoppingList(string start, string end)
        {
            if (string.IsNullOrEmpty(start) || string.IsNullOrEmpty(end))
                return BadRequest("Missing date parameters.");

            try
            {
                var startDate = DateTime.Parse(start);
                var endDate = DateTime.Parse(end);

                IEnumerable<Ingredient> shoppingList = await _unitOfWork.GetShoppingList(startDate, endDate);

                byte[] bytes = GetFileStream(shoppingList);
                
                return new FileContentResult(bytes, 
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    {
                        FileDownloadName = $"{start} to {end} Shopping List.xlsx"
                    };
            }
            catch (Exception ex)
            {
                _logger.LogError($"Unable to create shopping list file: {ex.Message}");
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _logger.LogInformation("Retrieving recipe...");

                    //TODO: Add delete method in IRepository

                    _unitOfWork.DeleteScheduleRecipe(id);

                    var result = await _unitOfWork.SaveChangesAsync();

                    if (result)
                        return Ok("Recipe successfully removed from scheduled");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            _logger.LogError("Bad model request");
            return BadRequest("Please try again.");
        }

        private byte[] GetFileStream(IEnumerable<Ingredient> shoppingList)
        {
            byte[] bytes;

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Shopping List");
                int row = 1;

                worksheet.Cells["A1"].Value = "Amount";
                worksheet.Cells["B1"].Value = "Measurement";
                worksheet.Cells["C1"].Value = "Item";

                row++;

                foreach (var item in shoppingList)
                {
                    worksheet.Cells[$"A{row}"].Value = item.Amount;
                    worksheet.Cells[$"B{row}"].Value = item.Measurement.Name + (item.Amount == 1 ? "" : "s");
                    worksheet.Cells[$"C{row}"].Value = item.Name;

                    row++;
                }

                worksheet.Cells.AutoFitColumns();

                package.Save();

                using (MemoryStream stream = new MemoryStream())
                {
                    package.SaveAs(stream);
                    stream.Position = 0;
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RecipeSPA.DAL;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Controllers.API
{
    [Route("/api/ingredient")]
    [Authorize()]
    public class IngredientController : Controller
    {
        private ILogger<IngredientController> _logger;
        private IUnitOfWork _unitOfWork;

        public IngredientController(IUnitOfWork unitOfWork, ILogger<IngredientController> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        [HttpGet("names")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var names = (await _unitOfWork.GetAllIngredients()).Select(item => item.Name).Distinct();

                return Ok(names);
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to get recipe names: {0}", ex.Message);
                return Ok("");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]IngredientViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Ingredient ingredient = _unitOfWork.AddIngredient(vm);

                    await _unitOfWork.SaveChangesAsync();

                    return Ok(ingredient);
                }
                catch(DBReferenceNotFoundException ex)
                {
                    _logger.LogError($"{ex.ReferenceType.ToString()} with id: {vm.RecipeId} not found.");
                    return NotFound($"Recipe with id=[{vm.RecipeId}] not found: {ex.Message}");
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Failed to add ingredient: {ex.Message}");
                    return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
                }
            }

            return BadRequest("Please check request body.");
        }

        [HttpDelete("~/api/recipes/{recipeId}/ingredient/{ingrId}")]
        public async Task<IActionResult> Delete(int recipeId, int ingrId)
        {
            try
            {
                _unitOfWork.DeleteIngredient(recipeId,ingrId);

                await _unitOfWork.SaveChangesAsync();

                return Ok("Ingredient deleted");
            }
            catch(DBReferenceNotFoundException ex)
            {
                _logger.LogError($"Ingredient delete failed. {ex.ReferenceType.ToString()} was not found.");
                return NotFound($"{ex.ReferenceType.ToString()} not found.");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            
        }
    }
}

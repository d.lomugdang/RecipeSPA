﻿using RecipeSPA.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Validators
{
    public class RecipeValidator : IRecipeValidator
    {
        public bool IsDuplicateRecipeName(IEnumerable<string> nameCollection, string recipeName)
        {
            return nameCollection.Contains(recipeName);
        }
    }
}

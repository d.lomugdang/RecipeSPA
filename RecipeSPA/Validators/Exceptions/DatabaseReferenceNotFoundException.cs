﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA
{
    public class DBReferenceNotFoundException : NullReferenceException
    {
        public Type ReferenceType { get; set; }

        public DBReferenceNotFoundException(string message,Type referenceType)
            : base(message)
        {
            ReferenceType = referenceType;
        }
    }
}

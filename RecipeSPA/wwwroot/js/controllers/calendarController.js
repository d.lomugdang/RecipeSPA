﻿(function () {
    'use strict';

    var app = angular.module('app');    

    var _calendarController = function ($scope, $http, $mdDialog, moment, calendarConfig) {

        var vm = this;

        vm.calendarView = 'month';
        vm.viewDate = new Date();
        vm.cellIsOpen = false;
        vm.events = populateEvents();

        function populateEvents() {

            $http.get("api/recipesched")
                .then(function (response) {
                    response.data.forEach(function (item) {

                        var event = createNewEvent(item.id, item.recipe, item.date);

                        vm.events.push(event);
                    });

                }, function (error) {

                });
        }

        function createNewEvent(id, event, date) {
            var color = {};

            if (event.color)
                color = event.color;
            else
                color = calendarConfig.colorTypes.custom;

            var newEvent = {
                'schedRecipeId': id,
                'recipeId': event.id,
                'title': event.name,
                'startsAt': new Date(date),
                'type': 'warning',
                'color': color,
                'actions': [{
                    label: '<i class=\'fa fa-trash-o\' style=\'color: white\'></i>',
                    onClick: function (args) {
                        removeMe(newEvent);
                    }
                }]
            };

            return newEvent;
        }

        function saveRecipeDate(event) {
            var url = "api/recipesched";

            var item = {
                'recipeId': event.recipeId,
                'date': event.startsAt.toISOString()
            };

            $http.post(url, item)
                .then(function (response) {
                    event.schedRecipeId = response.data;
                }, function (error) {
                    self.errorMessage = "Error occurred in the server: " + error;
                });
        }

        function removeMe(event) {

            removeFromDb(event.schedRecipeId);

            var index = vm.events.indexOf(event);

            vm.events.splice(index, 1);
        }

        function removeFromDb(schedId) {
            var url = "api/recipesched/" + schedId;

            $http.delete(url)
                .then(function (response) {

                }, function (error) {
                    self.errorMessage = "Error occurred in the server: " + error;
                });
        }

        $scope.$on('deleteRecipeSchedules', function (event, data) {

            var schedRecipes = vm.events.filter(function (item) {
                return item.recipeId === data;
            });

            schedRecipes.forEach(function (item) {                
                removeMe(item);
            });
        });

        vm.viewRecipe = function (event) {
            $scope.$emit('viewRecipe', event.recipeId);
        };

        vm.showShopModal = function () {
            $mdDialog.show({
                controller: shopController,
                controllerAs: 'sc',
                templateUrl: '../views/shopModal.html?bust=' + Math.random().toString(36).slice(2),
                parent: angular.element(document.getElementById('main')),
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen
            });
        };

        vm.eventDropped = function (event, start, end) {

            var newEvent = createNewEvent(0, event, start);

            saveRecipeDate(newEvent);

            vm.events.push(newEvent);

            event.startsAt = start;
            vm.viewDate = start;
            vm.cellIsOpen = true;
        };

        var shopController = function ($scope, $mdDialog, moment) {

            var sc = this;

            sc.end = moment().add(6, "days");

            sc.start = moment();

            sc.startOptions = {
                singleDatePicker: true,
                locale: {
                    format: "MMM D, YYYY"
                },
                eventHandlers: {
                    'hide.daterangepicker': function (ev, picker) {
                        sc.end = moment(ev.model).add(6, 'days');
                    }
                }
            };

            sc.endOptions = {
                singleDatePicker: true,
                locale: {
                    format: "MMM D, YYYY"
                },
                eventHandlers: {
                    'hide.daterangepicker': function (ev, picker) {
                        sc.start = moment(ev.model).subtract(6, 'days');
                    }
                }
            };

            sc.exportList = function () {
                var start = formatUriDate(sc.start);
                var end = formatUriDate(sc.end);

                var url = "/api/recipesched/shoppinglist?start=" + start + "&end=" + end;

                window.open(url, '_blank', '');

                $mdDialog.cancel();
            };

            function formatUriDate(date) {
                return moment(date).format('MM-DD-YYYY');
            }
        };
    };

    _calendarController.$inject = ['$scope', '$http', '$mdDialog', 'moment', 'calendarConfig'];

    app.controller('CalendarController', _calendarController);

})();


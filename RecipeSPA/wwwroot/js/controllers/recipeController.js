﻿(function () {
    'use strict';

    var app = angular.module('app');

    var _recipecontroller = function ($scope, $http, $mdDialog, $exceptionHandler, $q, $timeout, recipeData) {

        var self = this;
        const SAVE_NEW = 'Add Recipe';
        const SAVE_CURRENT = 'Close';

        self.isBusy = false;
        self.disableClose = false;

        $scope.measurements = [];

        $scope.ingredientNames = loadAllNames();

        $scope.simulateQuery = true;
        $scope.selectedItem = "";
        $scope.querySearch = querySearch;

        self.searchText = "";
        self.answer = {};
        self.message = "";
        self.errorMessage = "";
        self.editName = false;
        self.editSize = false;
        self.unchanged = {};
        self.promises = [];
        $scope.recipe = recipeData;

        self.isNewRecipe = function () {
            return $scope.recipe.id === 0 || $scope.recipe.id === undefined;
        };
        self.saveButtonText = self.isNewRecipe() ? SAVE_NEW : SAVE_CURRENT;

        getMeasurements();

        $scope.newIngredient = {};

        function getMeasurements() {
            self.isBusy = true;

            $http.get("/api/measurements")
                .then(function (response) {
                    angular.copy(response.data, $scope.measurements);
                    $scope.newIngredient['measurement'] = $scope.measurements[0];

                    $scope.newIngredient = {
                        'id': 0,
                        'amount': 1.00,
                        'measurement': $scope.measurements[indexOfObjWithAttr($scope.measurements,'name','cup')],
                        'name': ''
                    };
                }, function (error) {
                    self.errorMessage = "Failed to load database data. Returning hard values: " + error;
                    $scope.measurements = [{ 'id': '0', 'name': 'kilogram' }, { 'id': '0', 'name': 'piece' }, { 'id': '0', 'name': 'ounce' }];
                })
                .finally(function () {
                    self.isBusy = false;
                });
        }

        $scope.addIngredient = function () {
            var i = {
                'amount': $scope.newIngredient.amount,
                'measurement': $scope.newIngredient.measurement,
                'name': ''
            };

            if ($scope.newIngredient.name) {
                i.name = $scope.newIngredient.name.display === undefined ? $scope.newIngredient.name : $scope.newIngredient.name.display;
            } else {
                i.name = self.searchText;
            }

            if (i.name === '' || i.name === undefined)
                return;

            if (i.amount === 0 || i.amount === undefined)
                return;

            if (!self.isNewRecipe()) {
                saveIngredient(i);
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.save = function () {
            var recipeObj = buildRecipeObj();

            sendToServer(recipeObj);            
        };

        $scope.confirmRemoveIngredient = function (chip) {
            self.disableClose = true;

            $mdDialog.show({
                controllerAs: 'confirmCtrl',
                controller: function ($mdDialog) {
                    this.answer = function (answer) {
                        $mdDialog.hide(answer);
                    };
                },
                multiple: true,
                templateUrl: '../views/confirmDialog.html'
            }).then(function (answer) {
                if (answer === 'yes') {
                    deleteIngredient(chip.id);
                } else {
                    $scope.recipe.ingredients.push(chip);
                }
            })
                .finally(function () {
                    self.disableClose = false;
                });
        };

        self.initText = function () {
            if (self.isNewRecipe)
                return 'Enter recipe name';
            else
                return recipe.name;
        };

        function deleteIngredient(id) {
            var url = "/api/recipes/" + $scope.recipe.id + "/ingredient/" + id;

            $http.delete(url)
                .then(function (success) {
                    console.log(success);
                }, function (error) {
                    console.log(error);
                });
        }

        function saveIngredient(ingredient) {
            ingredient.recipeId = $scope.recipe.id;

            $http.post("/api/ingredient", ingredient)
                .then(function (response) {
                    self.message = response;
                    delete ingredient.recipeId;
                    ingredient.id = response.data.id;
                    $scope.recipe.ingredients.push(ingredient);
                    $scope.$emit('updateRecipe', $scope.recipe);
                }, function (error) {
                    self.errorMessage = error;
                });
        }

        function postToServer(data) {
            self.isBusy = true;

            var url = "/api/recipes";

            $http.post(url, data)
                .then(function (response) {
                    angular.copy(response.data, self.answer);
                    $mdDialog.hide(self.answer);
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#main')))
                            .clickOutsideToClose(true)
                            .title('New Recipe Added')
                            .textContent(response.data['name'] + ' was successfully added.')
                            .ariaLabel('Recipe Added')
                            .ok('Great!'));
                    }, function (error) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .title('Request Failed')
                                .textContent("Unable to add recipe: " + error.data)
                                .ok('Try again')
                        );
                    })
                .finally(function () {
                        self.isBusy = false;
                });
        }

        function buildRecipeObj() {
            var recipe = {
                'id': $scope.recipe.id,
                'name': $scope.recipe.name,
                'servingSize': $scope.recipe.servingSize,
                'ingredients': $scope.recipe.ingredients
            };

            return recipe;
        }

        function loadAllNames() {
            self.isBusy = true;

            var results = [];

            $http.get("/api/ingredient/names")
                .then(function (response) {
                    var allNames = response.data;

                    angular.forEach(allNames, function (ingredient, key) {
                        results.push({
                            value: ingredient.toLowerCase(),
                            display: ingredient
                        });
                    });

                }, function (error) {
                    self.errorMessage = "Failed to load data: " + error;
                })
                .finally(function () {
                    self.isBusy = false;
                });

            return results;
        }

        function querySearch(query) {
            var results = query ? $scope.ingredientNames.filter(createFilterFor(query)) : $scope.ingredientNames, deferred;

            self.searchText = query;

            if ($scope.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () {
                    deferred.resolve(results);
                }, Math.random() * 500, false);
                return deferred.promise;
            }
            else {
                return results;
            }
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(ingredient) {
                return ingredient.value.indexOf(lowercaseQuery) >= 0;
            };
        }

        function saveRecipe() {
            self.isBusy = true;

            var url = "/api/recipes";

            $http.post(url, data)
                .then(function (response) {
                    angular.copy(response.data, self.answer);
                }, function (error) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .title('Request Failed')
                            .textContent("Unable to add recipe: " + error.data)
                            .ok('Try again')
                    );
                })
                .finally(function () {
                    self.isBusy = false;
                });
        }

        self.edit = function (field) {
            switch (field) {
                case 'name':
                    self.editName = true;
                    self.unchanged['name'] = $scope.recipe.name;
                    break;
                case 'servingSize':
                    self.editSize = true;
                    self.unchanged['servingSize'] = $scope.recipe.servingSize;
                    break;
            }
        };

        self.cancelEdit = function (field) {
            switch (field) {
                case 'name':
                    $scope.recipe.name = self.unchanged['name'];
                    break;
                case 'servingSize':
                    $scope.recipe.servingSize = self.unchanged['servingSize'];
                    break;
            }

            self.disableEdit(field);
        };
        self.disableEdit = function (field) {
            switch (field) {
                case 'name':
                    self.editName = false;
                    break;
                case 'servingSize':
                    self.editSize = false;
                    break;
            }
        }

        self.saveChange = function (field) {
            console.log('lost focus');

            if ($scope.recipe.id === 0 || $scope.recipe.id === undefined || field === 'new') {
                postToServer(buildRecipeObj());
            } else {
                var url = "/api/recipes/" + $scope.recipe.id;
                var data = {};
                data[field] = $scope.recipe[field];

                var update = {};

                update = $http.patch(url, data)
                    .then(function (success) {
                        self.unchanged[field] = success.data[field];
                        self.answer = success.data;
                        self.promises.splice(self.promises.indexOf(update), 1);
                    }, function (error) {
                        console.log(error);
                    });

                self.promises.push(update);
            }

            self.disableEdit(field);
        };

        self.closeModal = function () {
            console.log('clicked');

            if (self.isNewRecipe()) {
                self.saveChange('new');
            } else {
                if (self.promises.length > 0) {
                    $q.all(self.promises).then(function () {
                        $mdDialog.hide(self.answer);
                    });
                } else {
                    $mdDialog.hide(self.answer);
                }
            }
        };
    };

    _recipecontroller.$inject = ['$scope', '$http', '$mdDialog', '$exceptionHandler', '$q', '$timeout', 'recipeData'];

    app.controller('RecipeController', _recipecontroller);

})();
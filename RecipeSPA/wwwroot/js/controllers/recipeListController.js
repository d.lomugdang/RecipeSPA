﻿(function () {
    'use strict';

    var app = angular.module('app');

    var _recipeListController = function ($scope, $rootScope, $mdDialog, $http, $q, $timeout, moment, calendarConfig, pager) {

        var self = this;

        self.errorMessage = "";
        self.isBusy = true;
        self.searchText = "";


        self.calendarView = 'month';
        self.viewDate = moment().startOf('month').toDate();
        self.cellIsOpen = false;
        self.viewCountOptions = [3, 5, 10];
        self.viewCount = 3;

        self.recipeList = [];
        self.partialList = [];
        self.events = [];
        self.conversions = [];
        self.newRecipe = {
            'ingredients': []
        };

        self.recipe = {};
        self.itemList = [];
        self.pageCache = [];

        $scope.pagerBusy = false;

        function setRecipeData(recipe) {
            var toUpdate = self.recipeList.filter(function (obj) {
                return obj.id === recipe.id;
            })[0];

            var index = self.recipeList.indexOf(toUpdate);

            if (toUpdate) {
                var modIngredients = [];

                recipe.ingredients.forEach(function (ingr) {

                    var modIngr = {
                        'id': ingr.id,
                        'amount': ingr.amount,
                        'name': ingr.name,
                        'measurement': ingr.measurement
                    };

                    modIngredients.push(modIngr);
                });

                toUpdate = {
                    'id': recipe.id,
                    'name': recipe.name,
                    'servingSize': recipe.servingSize,
                    'ingredients': modIngredients
                };
            }

            angular.copy(toUpdate, self.recipeList[index]);
        }

        self.searchRecipe = function () {
            var filteredList = self.recipeList.filter(function (item) {
                return item.name.toLowerCase().includes(self.searchText.toLowerCase());
            });

            angular.copy(filteredList, self.itemList);

            self.setPage(1);
        };

        self.clear = function () {

            angular.copy(self.recipeList, self.itemList);

            self.searchText = "";

            self.setPage(1);
        };

        self.populateRecipes = function () {

            self.isBusy = true;

            $http.get("/api/recipes")
                .then(function (response) {
                    //Success                
                    angular.copy(response.data, self.recipeList);
                    self.recipeList.forEach(function (item) {
                        item.name = item.name;
                        item.type = 'warning';
                        item.color = calendarConfig.colorTypes.custom;
                        item.draggable = true;
                        item.startsAt = moment().startOf('month').toDate();                        
                    });

                    self.recipeList.sort(function (a, b) {
                        if (a.name < b.name) {
                            return -1;
                        }
                        if (a.name > b.name) {
                            return 1;
                        }
                        return 0;
                    });

                    angular.copy(self.recipeList,self.itemList);
                    cachePaging();
                    self.getPage(1);
                    

                }, function (error) {
                    self.errorMessage = "Failed to load data: " + error;
                }).finally(function () {
                    self.isBusy = false;
                });
        };

        self.retrieveUserConfig = function () {
            $http.get("/api/conversion/config")
                .then(function (response) {
                    angular.copy(response.data, self.conversions);
                }, function (error) {

                });
        };

        self.getRecipeData = function (id) {
            var item = self.recipeList.filter(function (obj) {
                return obj.id === id;
            })[0];

            if (item) {
                var modIngredients = [];

                item.ingredients.forEach(function (ingr) {

                    var modIngr = {
                        'id': ingr.id,
                        'amount': ingr.amount,
                        'name': ingr.name,
                        'measurement': ingr.measurement
                    };

                    modIngredients.push(modIngr);
                });

                self.recipe = {
                    'id': item.id,
                    'name': item.name,
                    'servingSize': item.servingSize,
                    'ingredients': modIngredients
                };
            }
        };

        self.confirmDelete = function (id) {
            self.getRecipeData(id);

            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to delete ' + self.recipe.name + '?')
                .textContent('Recipe will also be removed from any scheduled date.')
                .ok('Delete')
                .cancel('Nevermind');

            $mdDialog.show(confirm).then(function () {
                deleteRecipe(self.recipe.id, indexOfObjWithAttr(self.recipeList, 'id', self.recipe.id));

                self.setPage(1);

            }, function () {

            });
        };

        function deleteRecipe(id, index) {
            $scope.$broadcast('deleteRecipeSchedules', id);

            deleteRecipeFromDb(id);
            updateLists(index);
        }

        function updateLists(index) {
            self.recipeList.splice(index, 1);
            self.itemList.splice(index, 1);
        }

        function deleteRecipeFromDb(id) {
            var url = "/api/recipes/" + id;

            $http.delete(url)
                .then(function (response) {

                }, function (error) {
                    self.errorMessage = "Failed to load data: " + error;
                })
                .finally(function () {
                    self.isBusy = false;
                });
        }

        self.showRecipe = function (id) {
            self.getRecipeData(id);

            $mdDialog.show({
                locals: { recipeData: self.recipe },
                controller: 'RecipeController',
                templateUrl: '../views/recipeModal.html?bust=' + Math.random().toString(36).slice(2),
                parent: angular.element(document.getElementById('main')),
                controllerAs: 'recipeCtrl',
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (answer) {
                    setRecipeData(answer);
                }, function () {

                });
        };

        self.showConfigModal = function () {
            $mdDialog.show({                
                locals: { configData: self.conversions },
                controller: 'ConversionController',
                templateUrl: '../views/configModal.html?bust=' + Math.random().toString(36).slice(2),
                parent: angular.element(document.getElementById('main')),
                controllerAs: 'conv',
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (answer) {
                    $scope.$apply();
                    self.$apply();
                }, function () {

                });
        };

        self.addNewRecipe = function () {
            $mdDialog.show({
                locals: { recipeData: self.newRecipe },
                controller: 'RecipeController',
                templateUrl: '../views/recipeModal.html?bust=' + Math.random().toString(36).slice(2),
                parent: angular.element(document.getElementById('main')),
                controllerAs: 'recipeCtrl',
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (answer) {
                    self.recipeList.push(answer);
                    self.itemList.push(answer);
                    self.setPage(1);
                }, function () {

                });
        };

        self.pager = {};

        var cachePaging = function () {
            self.pageCache.push(self.setPage(1));

            for (var i = 2; i <= self.pager.totalPages; i++) {
                self.pageCache.push(self.setPage(i));
            }
        };

        self.getPage = function (page) {
            self.isBusy = true;

            self.partialList = [];

            $timeout(function () {

                self.pageCache[page - 1].forEach(function (item) {
                    self.partialList.push(item);
                });

                self.pager.currentPage = page;

            }, 500).then(function () {
                self.isBusy = false;
            });
        };

        self.setPage = function (page) {

            if (page < 1 || page > self.pager.totalPages) {
                return;
            }            

            self.pager = pager.GetPager(self.itemList.length, page, self.viewCount);

            return self.itemList.slice(self.pager.startIndex, self.pager.endIndex + 1);
        };

        self.updateRecipe = function (recipe) {
            var i = indexOfObjWithAttr(self.recipeList, 'id', recipe.id);

            self.recipeList[i] = recipe;
            self.itemList[i] = recipe;
        };

        self.init = function () {
            self.retrieveUserConfig();
            self.populateRecipes();
        };

        self.init();

        $scope.$on('viewRecipe', function (event, recipeId) {
            self.showRecipe(recipeId);
        });

        $rootScope.$on('updateRecipe', function (event, recipe) {
            self.updateRecipe(recipe);            
        });
    };

    _recipeListController.$inject = ['$scope', '$rootScope','$mdDialog', '$http', '$q', '$timeout', 'moment', 'calendarConfig', 'pager'];

    app.controller('RecipeListController', _recipeListController);
})();
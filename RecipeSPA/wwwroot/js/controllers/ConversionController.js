﻿(function () {
    'use strict';

    var app = angular.module('app');

    var _conversionController = function ($scope, $mdDialog, $exceptionHandler, configData) {
        var conv = this;

        conv.conversions = configData;
        conv.displayMode = true;

        console.log(conv.conversions);
    };

    _conversionController.$inject = ['$scope', '$mdDialog', '$exceptionHandler', 'configData'];

    app.controller('ConversionController', _conversionController);
})();

﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.directive('spInput', function () {
        return {
            restrict: 'E',
            scope: {
                inputText: '=',
                inputClass: '@',
                showConfirm: '@',
                showCancel: '@',
                optionsClass: '@',
                triggerFocus: '=',
                inputId: '=',
                placeholderValue: '@',
                onCancel: '&?',
                onBlur: '&?',
                onConfirm: '&?'
            },
            controllerAs: 'iCtrl',
            controller: function ($scope) {
                var self = this;
                self.inputPadding = $scope.onCancel && $scope.onConfirm ? 'padding-right: 2em;' : 'padding-right: 1.4em;';
            },
            templateUrl: '../views/specialInput.html'
        };
    });

    app.directive('focusIn', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.$watch(attr.focusIn, function (n, o) {
                    if (n === true) {
                        setTimeout(function () {
                            element[0].focus();
                        }, 1);
                    }
                });
            }
        };
    });
})();
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RecipeSPA.Models;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Security.Claims;
using RecipeSPA.Interfaces;
using RecipeSPA.Validators;
using RecipeSPA.DAL;
using RecipeSPA.Models.ViewModels;

namespace RecipeSPA
{
    public class Startup
    {
        private IHostingEnvironment _env;
        private IConfigurationRoot _config;

        public OAuthOptions GitHubOptions
        {
            get
            {
                return new OAuthOptions
                {
                    AuthenticationScheme = "GitHub",
                    DisplayName = "GitHub",
                    ClientId = _config["Authentication:GitHub:ClientID"],
                    ClientSecret = _config["Authentication:GitHub:ClientSecret"],
                    CallbackPath = new PathString("/signin-github"),
                    AuthorizationEndpoint = "https://github.com/login/oauth/authorize",
                    TokenEndpoint = "https://github.com/login/oauth/access_token",
                    UserInformationEndpoint = "https://api.github.com/user",
                    ClaimsIssuer = "OAuth2-Github",
                    SaveTokens = true,

                    // Retrieving user information is unique to each provider.
                    Events = new OAuthEvents
                    {
                        OnCreatingTicket = async context => { await CreatingGitHubAuthTicket(context); }
                    }
                };
            }
        }

        private static async Task CreatingGitHubAuthTicket(OAuthCreatingTicketContext context)
        {
            // Get the GitHub user
            var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await context.Backchannel.SendAsync(request, context.HttpContext.RequestAborted);
            response.EnsureSuccessStatusCode();

            var user = JObject.Parse(await response.Content.ReadAsStringAsync());

            AddClaims(context, user);
        }

        private static void AddClaims(OAuthCreatingTicketContext context, JObject user)
        {
            var identifier = user.Value<string>("id");
            if (!string.IsNullOrEmpty(identifier))
            {
                context.Identity.AddClaim(new Claim(
                    ClaimTypes.NameIdentifier, identifier,
                    ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            var userName = user.Value<string>("login");
            if (!string.IsNullOrEmpty(userName))
            {
                context.Identity.AddClaim(new Claim(
                    ClaimsIdentity.DefaultNameClaimType, userName,
                    ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            var name = user.Value<string>("name");
            if (!string.IsNullOrEmpty(name))
            {
                context.Identity.AddClaim(new Claim(
                    "urn:github:name", name,
                    ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            var link = user.Value<string>("url");
            if (!string.IsNullOrEmpty(link))
            {
                context.Identity.AddClaim(new Claim(
                    "urn:github:url", link,
                    ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }
        }

        public Startup(IHostingEnvironment env)
        {
            _env = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(_env.ContentRootPath)
                .AddJsonFile("config.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
                

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();

            _config = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(_config);

            services.AddDbContext<RecipeContext>();

            services.AddScoped<IUnitOfWork,UnitOfWork>();
            services.AddScoped<IBaseRepository<Recipe>, RecipeRepository>();
            services.AddScoped<IBaseRepository<Ingredient>, IngredientRepository>();
            services.AddScoped<IBaseRepository<Measurement>, MeasurementRepository>();
            services.AddScoped<IBaseRepository<Conversion>, ConversionRepository>();
            services.AddScoped<IBaseRepository<ScheduledRecipe>, ScheduledRecipeRepository>();
            services.AddScoped<IRecipeValidator, RecipeValidator>();

            services.AddIdentity<RecipeUser, IdentityRole>(config =>
             {
                 config.User.RequireUniqueEmail = true;
                 config.Password.RequiredLength = 8;
                 config.Cookies.ApplicationCookie.LoginPath = "/Auth/Login";
                 config.Cookies.ApplicationCookie.Events = new CookieAuthenticationEvents()
                 {
                     OnRedirectToLogin = async ctx =>
                     {
                         if (ctx.Request.Path.StartsWithSegments("/api") &&
                                ctx.Response.StatusCode == 200)
                         {
                             ctx.Response.StatusCode = 401;
                         }
                         else
                         {
                             ctx.Response.Redirect(ctx.RedirectUri);
                         }

                         await Task.Yield();
                     }
                 };
             })
             .AddEntityFrameworkStores<RecipeContext>();

            services.AddLogging();

            services.AddTransient<RecipeContextSeedData>();

            services.AddMvc(config =>
            {
                config.SslPort = 44305;
                config.Filters.Add(new RequireHttpsAttribute());

            })
            .AddJsonOptions(config =>
            {
                config.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            if (!_env.IsDevelopment())
                services.Configure<MvcOptions>(o =>
                    o.Filters.Add(new RequireHttpsAttribute()));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, RecipeContextSeedData seeder)
        {
            loggerFactory.AddConsole();

            Mapper.Initialize(config =>
            {
                config.CreateMap<RecipeViewModel, Recipe>().ReverseMap();
                config.CreateMap<RecipePatchModel, Recipe>().ForMember(d => d.Name, opt => opt.MapFrom(src => src.Name));
                config.CreateMap<IngredientViewModel, Ingredient>().ForSourceMember(s => s.RecipeId, opt => opt.Ignore()).ReverseMap();
            });

            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentity();

            app.UseGoogleAuthentication(new GoogleOptions()
            {
                ClientId = _config["Authentication:Google:ClientId"],
                ClientSecret = _config["Authentication:Google:ClientSecret"]
            });

            app.UseOAuthAuthentication(GitHubOptions);

            app.UseMvc(config =>
            {
                config.MapRoute(
                  name: "Default",
                  template: "{controller}/{action}/{id?}",
                  defaults: new { controller = "App", action = "Index" }
                );
            });

            seeder.EnsureSeedData().Wait();
        }
    }
}

﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RecipeSPA.Models.ViewModels;

namespace RecipeSPA.DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private IBaseRepository<Recipe> RecipeRepo { get; set; }
        private IBaseRepository<Ingredient> IngredientRepo { get; set; }
        private IBaseRepository<Conversion> ConversionRepo { get; set; }
        private IBaseRepository<Measurement> MeasurementRepo { get; set; }
        private IBaseRepository<ScheduledRecipe> ScheduledRecipeRepo { get; set; }
        private RecipeContext Context { get; set; }

        private bool disposed = false;
        private Utils.Graph<Measurement> _graph;
        Utils.Graph<Measurement> ConvGraph
        {
            get
            {
                if ((object)_graph == null)
                {
                    var convList = ConversionRepo.GetAll();

                    List<Tuple<Measurement, Measurement>> edgeList = new List<Tuple<Measurement, Measurement>>();

                    foreach (var item in convList.Result)
                    {
                        edgeList.Add(new Tuple<Measurement, Measurement>(item.From, item.To));

                    }

                    _graph = new Graph<Measurement>(MeasurementRepo.GetAll().Result, edgeList);
                }

                return _graph;
            }
        }

        public UnitOfWork(IBaseRepository<Recipe> recipeRepo,
                            IBaseRepository<Ingredient> ingredientRepo,
                            IBaseRepository<Conversion> conversionRepo,
                            IBaseRepository<Measurement> measurementRepo,
                            IBaseRepository<ScheduledRecipe> schedRecipeRepo,
                            RecipeContext context)
        {
            this.RecipeRepo = recipeRepo;
            this.IngredientRepo = ingredientRepo;
            this.ConversionRepo = conversionRepo;
            this.MeasurementRepo = measurementRepo;
            this.ScheduledRecipeRepo = schedRecipeRepo;
            this.Context = context;
        }

        #region Private Methods
        private void UpdateRecipe(Recipe recipe, Recipe dbRecipe)
        {
            if(!string.IsNullOrEmpty(recipe.Name))
                dbRecipe.Name = recipe.Name;
            if((object)recipe.ServingSize != null || recipe.ServingSize != 0)
                dbRecipe.ServingSize = recipe.ServingSize;

            Context.Entry(dbRecipe).State = EntityState.Modified;
        }
        private void AddNewIngredients(Recipe recipe, Recipe dbRecipe)
        {
            var toCreate = recipe.Ingredients.Where(i => i.Id == 0);

            foreach (var ingredient in toCreate)
            {
                ingredient.Measurement = MeasurementRepo.GetById(ingredient.Measurement.Id);
                dbRecipe.Ingredients.Add(ingredient);
            }
        }
        private void UpdateIngredients(Recipe recipe, Recipe dbRecipe)
        {
            var toUpdate = dbRecipe.Ingredients.Where(client => recipe.Ingredients.Any(server => server.Id == client.Id));

            foreach (var ingredient in toUpdate)
            {
                var fromRequest = recipe.Ingredients.FirstOrDefault(i => i.Id == ingredient.Id);

                ingredient.Measurement = MeasurementRepo.GetById(fromRequest.Measurement.Id);
                ingredient.Amount = fromRequest.Amount;
                ingredient.Name = fromRequest.Name;
            }

            foreach (var item in toUpdate)
            {
                IngredientRepo.Update(item);
            }
        }
        private void DeleteIngredients(Recipe recipe, Recipe dbRecipe)
        {
            var toDelete = dbRecipe.Ingredients.Where(client => !recipe.Ingredients.Any(server => server.Id == client.Id));

            foreach (var item in toDelete)
            {
                IngredientRepo.Delete(item);
            }
        }
        private void SetTotalIngredientAmount(List<Ingredient> ingredients, ref List<Ingredient> shoppingList)
        {
            Ingredient listItem = ingredients[0];

            if (!listItem.Measurement.IsMetric)
            {
                ConvertToNearestMetricMeasurement(ref listItem);
            }

            foreach (var item in ingredients.Skip(1))
            {
                if (item.Measurement != listItem.Measurement)
                    DoConversion(GetConversionPath(item.Measurement, listItem.Measurement), ref listItem);
                else
                    listItem.Amount += item.Amount;

            }

            shoppingList.Add(listItem);
        }
        private void ConvertToNearestMetricMeasurement(ref Ingredient listItem)
        {
            Measurement metric = FindNearestMetricMeasurement(listItem.Measurement);

            if (metric == null)
                return;

            LinkedList<Measurement> conversionPath = GetConversionPath(listItem.Measurement, metric);

            DoConversion(conversionPath, ref listItem);
        }
        private void DoConversion(LinkedList<Measurement> path, ref Ingredient listItem)
        {
            Measurement from = new Measurement();
            Measurement to = new Measurement();

            foreach (var m in path)
            {
                if (m == path.First.Value)
                {
                    from = m;
                    continue;
                }

                to = m;

                Conversion cv = GetEquivalence(from, to);

                if (cv != null)
                {
                    listItem.Amount *= cv.Multiplier;
                    listItem.Measurement = to;
                }

                from = to;
            }
        }
        private LinkedList<Measurement> GetConversionPath(Measurement measurement, Measurement metric)
        {
            Utils.SearchUtils<Measurement> search = new SearchUtils<Measurement>();
            return (LinkedList<Measurement>)search.ShortestPathFunction(ConvGraph, measurement).Invoke(metric);
        }
        private Conversion GetEquivalence(Measurement from, Measurement to)
        {
            if (ConversionRepo.Find(conv => conv.From == from && conv.To == to).Result.Count() > 0)
            {
                return ConversionRepo.GetAll().Result.FirstOrDefault(conv => conv.From == from && conv.To == to);
            }
            else if (ConversionRepo.Find(conv => conv.From == to && conv.To == from).Result.Count() > 0)
            {
                double multiplier = 1 / ConversionRepo.Find(conv => conv.From == to && conv.To == from).Result.Select(x => x.Multiplier).First();
                return new Conversion { From = to, To = from, Multiplier = multiplier };
            }
            else
                return null;
        }
        private Measurement FindNearestMetricMeasurement(Measurement start)
        {
            var visited = new HashSet<Measurement>();

            if (!ConvGraph.AdjacencyList.ContainsKey(start) || ConvGraph.AdjacencyList[start].Count == 0)
                return start;

            var queue = new Queue<Measurement>();
            queue.Enqueue(start);

            bool found = false;

            while (queue.Count > 0 || !found)
            {
                var vertex = queue.Dequeue();

                if (visited.Contains(vertex))
                    continue;

                visited.Add(vertex);

                foreach (var neighbor in ConvGraph.AdjacencyList[vertex])
                {
                    if (neighbor.IsMetric)
                    {
                        return neighbor;
                    }

                    if (!visited.Contains(neighbor))
                        queue.Enqueue(neighbor);
                }
            }

            return start;
        }
        #endregion

        public async Task<bool> SaveChangesAsync()
        {
            return (await Context.SaveChangesAsync()) > 0;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<IEnumerable<Recipe>> GetAllRecipesByUser(RecipeUser user)
        {
            return (await RecipeRepo.Find(r => r.User == user)).ToList();
        }        
        public Recipe AddRecipe(Recipe newRecipe)
        {
            return RecipeRepo.Add(newRecipe);
        }
        public void DeleteRecipe(int id)
        {
            DeleteRecipe(RecipeRepo.GetById(id));
        }
        public void DeleteRecipe(Recipe recipe)
        {
            RecipeRepo.Delete(recipe);
        }
        public Recipe UpdateRecipe(RecipePatchModel newValues, int id)
        {
            var dbRecipe = RecipeRepo.GetById(id);

            if(dbRecipe == null)
            {
                throw new DBReferenceNotFoundException($"Recipe with ID:{id} not found.", typeof(Recipe));
            }
            else
            {
                UpdateRecipe(Mapper.Map<Recipe>(newValues), dbRecipe);
            }

            return dbRecipe;
        }

        public async Task<IEnumerable<Ingredient>> GetShoppingList(DateTime startDate, DateTime endDate)
        {
            List<Ingredient> shoppingList = new List<Ingredient>();

            var ingredientsSet = await ScheduledRecipeRepo.Find(sr => sr.Date >= startDate && sr.Date <= endDate);

            var ingredientList = new List<Ingredient>();

            foreach (var set in ingredientsSet)
            {
                ingredientList.AddRange(set.Recipe.Ingredients);
            }

            List<List<Ingredient>> aggregated = new List<List<Ingredient>>();

            aggregated.AddRange(ingredientList.GroupBy(ingr => ingr.Name).Select(group => group.ToList()));

            foreach (var group in aggregated)
            {
                SetTotalIngredientAmount(group, ref shoppingList);
            }

            return shoppingList;
        }
        public async Task<IEnumerable<ScheduledRecipe>> GetScheduledRecipes(IEnumerable<int> recipes)
        {
            return (await ScheduledRecipeRepo.Find(r => recipes.Contains(r.Recipe.Id))).ToList();
        }
        public async Task<IEnumerable<Conversion>> GetAllConversions()
        {
            return (await ConversionRepo.GetAll()).ToList();
        }
        public async Task<IEnumerable<Ingredient>> GetAllIngredients()
        {
            return (await IngredientRepo.GetAll()).ToList();
        }
        public async Task<IEnumerable<Measurement>> GetAllMeasurements()
        {
            return (await MeasurementRepo.GetAll()).ToList();
        }

        public Recipe GetRecipeById(int recipeId)
        {
            return RecipeRepo.GetById(recipeId);
        }

        public ScheduledRecipe AddScheduledRecipe(ScheduledRecipe scheduledRecipe)
        {
            return ScheduledRecipeRepo.Add(scheduledRecipe);
        }

        public void DeleteScheduleRecipe(int id)
        {
            ScheduledRecipeRepo.Delete(ScheduledRecipeRepo.GetById(id));
        }

        public Ingredient AddIngredient(IngredientViewModel vm)
        {
            Ingredient igr = Mapper.Map<Ingredient>(vm);

            var recipe = RecipeRepo.GetById(vm.RecipeId);

            IngredientRepo.Add(igr);
            Context.Entry(igr.Measurement).State = EntityState.Unchanged;
            Context.Entry(recipe).State = EntityState.Modified;

            recipe.Ingredients.Add(igr);

            return igr;
        }

        public void DeleteIngredient(int recipeId, int ingrId)
        {
            var recipe = RecipeRepo.GetById(recipeId);

            if(recipe == null)
            {
                throw new DBReferenceNotFoundException($"Recipe with id:{recipeId} not found.", typeof(Recipe));
            }

            var ingredient = IngredientRepo.GetById(ingrId);

            if (ingredient == null)
            {
                throw new DBReferenceNotFoundException($"Ingredient with id:{ingrId} not found.", typeof(Ingredient));
            }

            recipe.Ingredients.Remove(ingredient);            

            Context.Entry(ingredient).State = EntityState.Deleted;
            Context.Entry(recipe).State = EntityState.Modified;
            
            //recipe.Ingredients.Remove(ingredient);
            //IngredientRepo.Delete(ingredient);

        }
    }
}
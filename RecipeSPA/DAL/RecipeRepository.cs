﻿using Microsoft.Extensions.Logging;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace RecipeSPA.DAL
{
    public class RecipeRepository : BaseRepository<Recipe>
    {        
        private IRecipeValidator _recipeValidator;        

        public RecipeRepository(RecipeContext context, ILogger<Conversion> _logger, IRecipeValidator recipeValidator) : base(context, _logger)
        {
            _recipeValidator = recipeValidator;  
        }

        public override Task<IEnumerable<Recipe>> Find(Expression<Func<Recipe, bool>> predicate)
        {
            Logger.LogInformation("Getting all recipes from database..");

            return Task.Run<IEnumerable<Recipe>>(() => Context.Recipes.Include(r => r.Ingredients).ThenInclude(i => i.Measurement).Where(predicate));
        }

        public override Recipe GetById(int id)
        {
            return Context.Recipes
                  .Include(t => t.Ingredients)
                  .Where(t => t.Id == id)
                  .FirstOrDefault();
        }

        public override Recipe Add(Recipe entity)
        {
            if (_recipeValidator.IsDuplicateRecipeName(Context.Recipes.Where(r => r.User == entity.User).Select(r => r.Name).ToList(), entity.Name))
                throw new DuplicateRecipeNameException($"There's a recipe already named {entity.Name}");

            foreach(var item in entity.Ingredients)
            {
                Context.Entry<Measurement>(item.Measurement).State = EntityState.Unchanged;
            }

            Context.Recipes.Add(entity);

            return entity;
        }
    }
}

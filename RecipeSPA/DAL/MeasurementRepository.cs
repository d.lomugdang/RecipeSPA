﻿using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace RecipeSPA.DAL
{
    public class MeasurementRepository : BaseRepository<Measurement>
    {
        public MeasurementRepository(RecipeContext context, ILogger<Conversion> _logger) : base(context, _logger)
        {
        }
    }
}

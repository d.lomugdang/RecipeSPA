﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RecipeSPA.Interfaces;
using RecipeSPA.Models;
using RecipeSPA.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace RecipeSPA.DAL
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected ILogger Logger { get; set; }
        protected RecipeContext Context { get; set; }

        public BaseRepository(RecipeContext context, ILogger logger)
        {
            this.Logger = logger;
            this.Context = context;
        }

        public virtual async Task<bool> SaveChanges()
        {
            return (await Context.SaveChangesAsync()) > 0;
        }

        public virtual IQueryable<TEntity> GetQuery()
        {
            return Context.Set<TEntity>();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            return await Task.Run(() => Context.Set<TEntity>().ToList());
        }

        public virtual async Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return await Task.Run(() => GetQuery().Where(predicate).ToList());
        }

        public virtual int Count()
        {
            return GetQuery().Count();
        }

        public virtual bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().Any(predicate);
        }

        public virtual TEntity GetById(int id)
        {
            return Context.Set<TEntity>().FirstOrDefault(entity => entity.Id == id);
        }

        public virtual TEntity Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);

            return entity;
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void Update(TEntity entity)
        {
            Context.Set<TEntity>().Update(entity);
        }
    }
}

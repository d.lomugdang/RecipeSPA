﻿using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace RecipeSPA.DAL
{
    public class ConversionRepository : BaseRepository<Conversion>
    {
        public ConversionRepository(RecipeContext context, ILogger<Conversion> _logger) : base(context, _logger)
        {
        }

        public override async Task<IEnumerable<Conversion>> GetAll()
        {
            return await Task.Run(() => Context.Set<Conversion>().Include(c => c.From).Include(c => c.To).ToList());
        }
    }
}

﻿using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace RecipeSPA.DAL
{
    public class IngredientRepository : BaseRepository<Ingredient>
    {
        public IngredientRepository(RecipeContext context, ILogger<Conversion> _logger) : base(context, _logger)
        {

        }

        public override Task<IEnumerable<Ingredient>> GetAll()
        {
            return Task.Run<IEnumerable<Ingredient>>(() => Context.Ingredients.ToList());
        }

        public override Ingredient GetById(int id)
        {
            return Context.Ingredients.Where(i => i.Id == id).Include(i => i.Measurement).FirstOrDefault();
        }
    }
}

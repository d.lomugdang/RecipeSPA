﻿using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using RecipeSPA.Utils;
using Microsoft.Extensions.Configuration;
using System.Linq.Expressions;

namespace RecipeSPA.DAL
{
    public class ScheduledRecipeRepository : BaseRepository<ScheduledRecipe>
    {
        public ScheduledRecipeRepository(RecipeContext context, ILogger<Conversion> _logger) : base(context, _logger)
        {

        }

        public override async Task<IEnumerable<ScheduledRecipe>> Find(Expression<Func<ScheduledRecipe, bool>> predicate)
        {
            return await Task.Run<IEnumerable<ScheduledRecipe>>(() => Context.ScheduledRecipes.Where(predicate).Include(scr => scr.Recipe).ThenInclude(rcp => rcp.Ingredients).ThenInclude(rcp => rcp.Measurement));

        }
    }
}

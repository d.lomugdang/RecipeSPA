﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models.ViewModels
{
    public class RecipePatchModel
    {
        [StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }
        [Range(1.0, Double.MaxValue)]
        public int? ServingSize { get; set; }
    }
}

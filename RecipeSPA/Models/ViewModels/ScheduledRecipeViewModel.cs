﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class ScheduledRecipeViewModel
    {
        [Required]
        public int RecipeId { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }
}

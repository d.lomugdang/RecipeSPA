﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models.ViewModels
{
    public class IngredientViewModel
    {
        [Required]
        public int RecipeId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Amount { get; set; }
        [Required]
        public Measurement Measurement { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models.ViewModels
{
    public class RecipeViewModel
    {
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }
        [Required]
        [Range(1.0,Double.MaxValue)]
        public double ServingSize { get; set; }
        public ICollection<Ingredient> Ingredients { get; set; }
    }
}

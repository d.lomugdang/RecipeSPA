﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecipeSPA.Interfaces;
using RecipeSPA.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RecipeSPA.Models
{
    public class RecipeRepositoryV1 
    {
        
        private ILogger<RecipeRepositoryV1> _logger;
        Utils.Graph<Measurement> _graph;

        Utils.Graph<Measurement> ConvGraph
        {
            get
            {
                if((object)_graph == null)
                {
                    var convList = _context.Conversions.Include(c => c.From).Include(c => c.To);

                    List<Tuple<Measurement, Measurement>> edgeList = new List<Tuple<Measurement, Measurement>>();

                    foreach (var item in convList)
                    {
                        edgeList.Add(new Tuple<Measurement, Measurement>(item.From, item.To));

                    }

                    _graph = new Graph<Measurement>(_context.Measurements, edgeList);
                }

                return _graph;
            }
        }
        private IRecipeValidator _recipeValidator;

        public RecipeContext _context { get; private set; }

        public RecipeRepositoryV1(RecipeContext context, ILogger<RecipeRepositoryV1> logger, IRecipeValidator recipeValidator)
        {
            _context = context;
            _logger = logger;
            _recipeValidator = recipeValidator;
        }

        public void AddIngredient(int id, string username,Ingredient newIngredient)
        {
            var recipe = GetRecipeById(id);

            if (recipe != null)
            {
                recipe.Ingredients.Add(newIngredient);
            }
        }

        //public async Task<Recipe> AddRecipe(Recipe recipe, string username)
        //{
        //    _logger.LogInformation("Adding recipe to the database");

        //    CheckDuplicateName(GetAllRecipesByUsername(username), recipe.Name);

        //    recipe.User = username;

        //    _context.Recipes.Add(recipe);

        //    await SaveChangesAsync();

        //    return recipe;
        //}

        public virtual bool CheckDuplicateName(IEnumerable<Recipe> coll, string name)
        {
            return _recipeValidator.IsDuplicateRecipeName(coll.Select(item => item.Name), name);
        }

        public void DeleteRecipeById(int id,string username)
        {
            var recipe = GetRecipeById(id);

            if(recipe != null)
            {
                _context.RemoveRange(recipe.Ingredients);
                _context.Remove(recipe);
            }
        }

        private void DeleteRecipeIngredients(ICollection<Ingredient> ingrs)
        {
        }

        //public virtual IEnumerable<Recipe> GetAllRecipesByUsername(string username)
        //{
        //    _logger.LogInformation("Getting all recipes from database..");

        //    return _context.Recipes.Include(r => r.Ingredients).ThenInclude(i => i.Measurement).Where(r => r.User == username).ToList();
        //}

        public Recipe GetRecipeById(int id)
        {
            return _context.Recipes
                  .Include(t => t.Ingredients)
                  .Where(t => t.Id == id)
                  .FirstOrDefault();
        }

        public virtual async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public bool UpdateRecipe(int id, Recipe item)
        {
            var dbRecipe = GetRecipeById(id);

            if (dbRecipe == null)
                return false;

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var toDelete = dbRecipe.Ingredients.Where(client => !item.Ingredients.Any(server => server.Id == client.Id));

                    _context.Ingredients.RemoveRange(toDelete);

                    var toUpdate = dbRecipe.Ingredients.Where(client => item.Ingredients.Any(server => server.Id == client.Id));

                    foreach (var ingredient in toUpdate)
                    {
                        var fromRequest = item.Ingredients.FirstOrDefault(i => i.Id == ingredient.Id);

                        ingredient.Measurement = _context.Measurements.SingleOrDefault(m => m.Id == fromRequest.Measurement.Id);
                        ingredient.Amount = fromRequest.Amount;
                        ingredient.Name = fromRequest.Name;
                    }

                    _context.Ingredients.UpdateRange(toUpdate);

                    var toCreate = item.Ingredients.Where(i => i.Id == 0);

                    foreach (var ingredient in toCreate)
                    {
                        ingredient.Measurement = _context.Measurements.SingleOrDefault(m => m.Id == ingredient.Measurement.Id);
                        dbRecipe.Ingredients.Add(ingredient);
                    }

                    dbRecipe.Name = item.Name;
                    dbRecipe.ServingSize = item.ServingSize;

                    _context.Entry(dbRecipe).State = EntityState.Modified;

                    _context.SaveChanges();

                    transaction.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex.InnerException;
                }
            }
        }

        public IEnumerable<Measurement> GetMeasurements()
        {
            _logger.LogInformation("Retreiving all measurements from database");

            return _context.Measurements.ToList();
        }

        public async Task<IEnumerable<string>> GetRecipeNames()
        {
            return await _context.Ingredients.Select(item => item.Name).ToListAsync();
        }

        public ScheduledRecipe AddScheduledRecipe(Recipe recipe, DateTime date)
        {
            ScheduledRecipe schRcp = new ScheduledRecipe { Recipe = recipe, Date = date };

            _logger.LogInformation("Adding recipe schedule to the database");

            _context.ScheduledRecipes.Add(schRcp);

            return schRcp;
        }

        //public IEnumerable<ScheduledRecipe> GetScheduledRecipesForUser(string name)
        //{
        //    var recipeIds = GetAllRecipesByUsername(name).Select(item => item.Id).Distinct();

        //    var scheduledRecipes = _context.ScheduledRecipes.Where(r => recipeIds.Contains(r.Recipe.Id));

        //    return scheduledRecipes;
        //}

        public void DeleteScheduledRecipe(int scheduledRecipeId)
        {
            var scheduledRecipe = _context.ScheduledRecipes.FirstOrDefault(item => item.Id == scheduledRecipeId);

            if ((object)scheduledRecipe != null)
                _context.ScheduledRecipes.Remove(scheduledRecipe);
        }

        public IEnumerable<Conversion> GetConversions()
        {
            return _context.Conversions.Include(c => c.To).Include(c => c.From);
        }

        public List<Ingredient> GetShoppingList(DateTime startDate, DateTime endDate)
        {
            List<Ingredient> shoppingList = new List<Ingredient>();

            var ingredientsSet = _context.ScheduledRecipes.Where(sr => sr.Date >= startDate && sr.Date <= endDate)
                                                            .Include(scr => scr.Recipe)
                                                            .ThenInclude(rcp => rcp.Ingredients)
                                                            .ThenInclude(rcp => rcp.Measurement);

            var ingredientList = new List<Ingredient>();

            foreach (var set in ingredientsSet)
            {
                ingredientList.AddRange(set.Recipe.Ingredients);
            }

            List<List<Ingredient>> aggregated = new List<List<Ingredient>>();

            aggregated.AddRange(ingredientList.GroupBy(ingr => ingr.Name).Select(group => group.ToList()));

            foreach (var group in aggregated)
            {
                SetTotalIngredientAmount(group, ref shoppingList);
            }

            return shoppingList;
        }

        private void SetTotalIngredientAmount(List<Ingredient> ingredients, ref List<Ingredient> shoppingList)
        {
            Ingredient listItem = ingredients[0];

            if(!listItem.Measurement.IsMetric)
            {
                ConvertToNearestMetricMeasurement(ref listItem);
            }

            foreach(var item in ingredients.Skip(1))
            {
                if (item.Measurement != listItem.Measurement)
                    DoConversion(GetConversionPath(item.Measurement, listItem.Measurement), ref listItem);
                else
                    listItem.Amount += item.Amount;

            }

            shoppingList.Add(listItem);
        }

        private void ConvertToNearestMetricMeasurement(ref Ingredient listItem)
        {
            Measurement metric = FindNearestMetricMeasurement(listItem.Measurement);

            if (metric == null)
                return;

            LinkedList<Measurement> conversionPath = GetConversionPath(listItem.Measurement, metric);

            DoConversion(conversionPath, ref listItem);
        }

        private LinkedList<Measurement> GetConversionPath(Measurement measurement, Measurement metric)
        {
            Utils.SearchUtils<Measurement> search = new SearchUtils<Measurement>();
            return (LinkedList<Measurement>)search.ShortestPathFunction(ConvGraph, measurement).Invoke(metric);
        }

        private void DoConversion(LinkedList<Measurement> path, ref Ingredient listItem)
        {
            Measurement from = new Measurement();
            Measurement to = new Measurement();

            foreach (var m in path)
            {
                if (m == path.First.Value)
                {
                    from = m;
                    continue;
                }

                to = m;

                Conversion cv = GetEquivalence(from, to);

                if(cv != null)
                {
                    listItem.Amount *= cv.Multiplier;
                    listItem.Measurement = to;
                }

                from = to;
            }
        }

        private Conversion GetEquivalence(Measurement from, Measurement to)
        {
            if (_context.Conversions.Where(conv => conv.From == from && conv.To == to).Count() > 0)
            {
                return _context.Conversions.FirstOrDefault(conv => conv.From == from && conv.To == to);
            }
            else if (_context.Conversions.Where(conv => conv.From == to && conv.To == from).Count() > 0)
            {
                double multiplier = 1 / _context.Conversions.Where(conv => conv.From == to && conv.To == from).Select(x => x.Multiplier).First();
                return new Conversion { From = to, To = from, Multiplier = multiplier };
            }
            else
                return null;
        }

        private Measurement FindNearestMetricMeasurement(Measurement start)
        {
            var visited = new HashSet<Measurement>();

            if (!ConvGraph.AdjacencyList.ContainsKey(start) || ConvGraph.AdjacencyList[start].Count == 0)
                return start;

            var queue = new Queue<Measurement>();
            queue.Enqueue(start);

            bool found = false;

            while (queue.Count > 0 || !found)
            {
                var vertex = queue.Dequeue();

                if (visited.Contains(vertex))
                    continue;

                visited.Add(vertex);

                foreach (var neighbor in ConvGraph.AdjacencyList[vertex])
                {
                    if (neighbor.IsMetric)
                    {
                        return neighbor;
                    }

                    if (!visited.Contains(neighbor))
                        queue.Enqueue(neighbor);
                }
            }

            return start;
        }
    }
}

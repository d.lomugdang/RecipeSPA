﻿using RecipeSPA.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class Conversion : BaseEntity
    {
        public double Multiplier { get; set; }

        public Measurement From { get; set; }
        public Measurement To { get; set; }
    }
}

﻿using RecipeSPA.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class ScheduledRecipe : BaseEntity
    {
        [Required]
        public Recipe Recipe { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }
}

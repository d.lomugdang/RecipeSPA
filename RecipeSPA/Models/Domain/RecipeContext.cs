﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RecipeSPA.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class RecipeContext : IdentityDbContext<RecipeUser>
    {
        public virtual DbSet<Recipe> Recipes { get; set; }
        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<ScheduledRecipe> ScheduledRecipes { get; set; }
        public virtual DbSet<Measurement> Measurements { get; set; }
        public virtual DbSet<Conversion> Conversions { get; set; }

        private IConfigurationRoot _config;

        public RecipeContext(IConfigurationRoot config)
        {
            _config = config;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Conversion>().HasOne(c => c.From).WithMany();
            modelBuilder.Entity<Conversion>().HasOne(c => c.To).WithMany();

            modelBuilder.Entity<Recipe>()
                       .Property(a => a.Timestamp)
                       .IsConcurrencyToken()
                       .ValueGeneratedOnAddOrUpdate();

            modelBuilder.Entity<Ingredient>()
                       .Property(a => a.Timestamp)
                       .IsConcurrencyToken()
                       .ValueGeneratedOnAddOrUpdate();

            modelBuilder.Entity<Measurement>()
                       .Property(a => a.Timestamp)
                       .IsConcurrencyToken()
                       .ValueGeneratedOnAddOrUpdate();

            modelBuilder.Entity<Conversion>()
                       .Property(a => a.Timestamp)
                       .IsConcurrencyToken()
                       .ValueGeneratedOnAddOrUpdate();

            modelBuilder.Entity<ScheduledRecipe>()
                       .Property(a => a.Timestamp)
                       .IsConcurrencyToken()
                       .ValueGeneratedOnAddOrUpdate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);            

            optionsBuilder.UseSqlServer(_config["ConnectionStrings:RecipeDBConnection"]);
        }
    }
}

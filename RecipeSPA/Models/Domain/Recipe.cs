﻿using RecipeSPA.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class Recipe : BaseEntity
    {
        public Recipe()
        {
            this.Ingredients = new HashSet<Ingredient>();
        }

        [Required]
        public string Name { get; set; }
        [Required]
        public int ServingSize { get; set; }
        [Required]
        public RecipeUser User { get; set; } 
        [Required]
        public ICollection<Ingredient> Ingredients { get; set; }        
    }
}

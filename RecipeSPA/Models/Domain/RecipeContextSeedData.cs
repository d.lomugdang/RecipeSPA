﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class RecipeContextSeedData
    {
        private RecipeContext _context;
        private UserManager<RecipeUser> _userManager;

        public RecipeContextSeedData(RecipeContext context, UserManager<RecipeUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task EnsureSeedData()
        {
            RecipeUser userOne = await _userManager.FindByEmailAsync("chef.curry@recipes.com");
            RecipeUser userTwo = await _userManager.FindByEmailAsync("gordon.ramsay@recipes.com");

            if(userOne == null)
            {
                userOne = new RecipeUser()
                {
                    UserName = "chefcurry",
                    Email = "chef.curry@recipes.com",                    
                };

                await _userManager.CreateAsync(userOne, "P@ssw0rd01");

                await _userManager.AddClaimAsync(userOne, new System.Security.Claims.Claim("RegularUser", userOne.Email));
            }

            if (userTwo == null)
            {
                userTwo = new RecipeUser()
                {
                    UserName = "gordonramsay",
                    Email = "gordon.ramsay@recipes.com"
                };

                await _userManager.CreateAsync(userTwo, "P@ssw0rd02");
                //await _userManager.AddToRoleAsync(user, "Power User");
            }

            Dictionary<string,Measurement> mList = new Dictionary<string, Measurement>();

            if(!_context.Measurements.Any())
            {
                mList.Add("ml", new Measurement { Name = "milliliter", Abbrv = "mL", IsMetric = true });
                mList.Add("tsp", new Measurement { Name = "teaspoon", Abbrv = "tsp", IsMetric = false });
                mList.Add("tbsp", new Measurement { Name = "tablespoon", Abbrv = "Tbsp", IsMetric = false });
                mList.Add("floz", new Measurement { Name = "fluid ounce", Abbrv = "fl oz", IsMetric = false });
                mList.Add("l", new Measurement { Name = "liter", Abbrv = "L", IsMetric = true });
                mList.Add("c", new Measurement { Name = "cup", Abbrv = "c", IsMetric = false });
                mList.Add("pt", new Measurement { Name = "pint", Abbrv = "pt", IsMetric = false });
                mList.Add("qt", new Measurement { Name = "quart", Abbrv = "qt", IsMetric = false });
                mList.Add("gal", new Measurement { Name = "gallon", Abbrv = "gal", IsMetric = false });
                mList.Add("lb", new Measurement { Name = "pounds", Abbrv = "lb", IsMetric = false });
                mList.Add("pc", new Measurement { Name = "piece", Abbrv = "pc", IsMetric = false });
                mList.Add("g", new Measurement { Name = "gram", Abbrv = "g", IsMetric = true });
                mList.Add("kg", new Measurement { Name = "kilogram", Abbrv = "kg", IsMetric = true });
                mList.Add("mg", new Measurement { Name = "milligram", Abbrv = "mg", IsMetric = true });
                mList.Add("oz", new Measurement { Name = "ounce", Abbrv = "oz", IsMetric = false });
            }            

            foreach(var item in mList)
            {
                _context.Measurements.Add(item.Value);
            }
            
            if(!_context.Conversions.Any())
            {
                _context.Conversions.Add(new Conversion { From = mList["tsp"], To = mList["ml"], Multiplier = 5 });
                _context.Conversions.Add(new Conversion { From = mList["tbsp"], To = mList["ml"], Multiplier = 15 });
                _context.Conversions.Add(new Conversion { From = mList["floz"], To = mList["ml"], Multiplier = 30 });
                _context.Conversions.Add(new Conversion { From = mList["c"], To = mList["ml"], Multiplier = 240 });
                _context.Conversions.Add(new Conversion { From = mList["l"], To = mList["ml"], Multiplier = 1000 });
                _context.Conversions.Add(new Conversion { From = mList["pt"], To = mList["c"], Multiplier = 2 });
                _context.Conversions.Add(new Conversion { From = mList["qt"], To = mList["pt"], Multiplier = 2 });
                _context.Conversions.Add(new Conversion { From = mList["gal"], To = mList["qt"], Multiplier = 4 });
                _context.Conversions.Add(new Conversion { From = mList["gal"], To = mList["l"], Multiplier = 3.8 });
                _context.Conversions.Add(new Conversion { From = mList["oz"], To = mList["g"], Multiplier = 28 });
                _context.Conversions.Add(new Conversion { From = mList["lb"], To = mList["oz"], Multiplier = 16 });
                _context.Conversions.Add(new Conversion { From = mList["kg"], To = mList["lb"], Multiplier = 2.205 });
                _context.Conversions.Add(new Conversion { From = mList["kg"], To = mList["g"], Multiplier = 1000 });
            }

            var macNCheese = new Recipe();
            var honeyGrilledChix = new Recipe();
            var frenchDipWich = new Recipe();

            if (!_context.Recipes.Any())
            {
                macNCheese = new Recipe
                {
                    Name = "Buffalo Mac and Cheese",
                    ServingSize = 2,
                    User = userOne,
                    Ingredients = new List<Ingredient>()
                    {
                        new Ingredient{ Amount = 6, Measurement = mList["tbsp"], Name = "all-purpose flour"},
                        new Ingredient{ Amount = 1, Measurement = mList["pc"], Name = "rotisserie-roasted chicken "},
                        new Ingredient{ Amount = 6, Measurement = mList["tbsp"], Name = "butter"},
                        new Ingredient{ Amount = 2, Measurement = mList["c"], Name = "shredded Cheddar cheese "},
                        new Ingredient{ Amount = 2, Measurement = mList["c"], Name = "shredded Monterey Jack cheese "}
                    },
                };

                _context.Recipes.Add(macNCheese);

                _context.Ingredients.AddRange(macNCheese.Ingredients);

                honeyGrilledChix = new Recipe
                {
                    Name = "Honey Mustard Grilled Chicken",
                    ServingSize = 4,
                    User = userOne,
                    Ingredients = new List<Ingredient>()
                    {
                        new Ingredient{ Amount = 0.33, Measurement = mList["c"], Name = "Dijon mustard"},
                        new Ingredient{ Amount = 0.25, Measurement = mList["c"], Name = "honey"},
                        new Ingredient{ Amount = 2, Measurement = mList["tbsp"], Name = "mayonnaise"},
                        new Ingredient{ Amount = 1, Measurement = mList["tsp"], Name = "steak sauce "},
                        new Ingredient{ Amount = 4, Measurement = mList["pc"], Name = "boneless chicken breast halves"}
                    },
                };

                _context.Recipes.Add(honeyGrilledChix);

                _context.Ingredients.AddRange(honeyGrilledChix.Ingredients);

                for(int i=0; i <= 20; i++)
                {
                    var rec = new Recipe
                    {
                        Name = $"Recipe {i}",
                        ServingSize = 4,
                        User = userOne,
                        Ingredients = new List<Ingredient>()
                        {
                            new Ingredient{ Amount = 0.33, Measurement = mList["c"], Name = "Dijon mustard"},
                            new Ingredient{ Amount = 0.25, Measurement = mList["c"], Name = "honey"},
                            new Ingredient{ Amount = 2, Measurement = mList["tbsp"], Name = "mayonnaise"},
                            new Ingredient{ Amount = 1, Measurement = mList["tsp"], Name = "steak sauce "},
                            new Ingredient{ Amount = 4, Measurement = mList["pc"], Name = "boneless chicken breast halves"}
                        },
                    };

                    _context.Recipes.Add(rec);

                    _context.Ingredients.AddRange(rec.Ingredients);
                }

                frenchDipWich = new Recipe
                {
                    Name = "Easy French Dip Sandwiches",
                    ServingSize = 4,
                    User = userTwo,
                    Ingredients = new List<Ingredient>()
                    {
                        new Ingredient{ Amount = 10.5, Measurement = mList["floz"], Name = "beef consomme"},
                        new Ingredient{ Amount = 8, Measurement = mList["pc"], Name = "provolone cheese"},
                        new Ingredient{ Amount = 1, Measurement = mList["c"], Name = "water"},
                        new Ingredient{ Amount = 4, Measurement = mList["pc"], Name = "hoagie rolls, split lengthwise"}
                    },
                };

                _context.Recipes.Add(frenchDipWich);

                _context.Ingredients.AddRange(frenchDipWich.Ingredients);

                _context.ScheduledRecipes.Add(new ScheduledRecipe { Recipe = macNCheese, Date = DateTime.Now.AddDays(2) });
                _context.ScheduledRecipes.Add(new ScheduledRecipe { Recipe = honeyGrilledChix, Date = DateTime.Now.AddDays(2) });
                _context.ScheduledRecipes.Add(new ScheduledRecipe { Recipe = frenchDipWich, Date = DateTime.Now.AddDays(2) });
            }

            if(_context.ScheduledRecipes.Any())
            {
                foreach(var item in _context.ScheduledRecipes)
                {
                    if(item.Date <= DateTime.Now)
                    {
                        item.Date = DateTime.Now.AddDays(2);
                    }
                }                
            }

            await _context.SaveChangesAsync();
            
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RecipeSPA.Models
{
    public class RecipeUser : IdentityUser
    {
        public string ChefLevel { get; set; }

    }
}
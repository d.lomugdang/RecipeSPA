﻿using RecipeSPA.Models.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RecipeSPA.Models
{
    public class Measurement : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Abbrv { get; set; }
        [Required]
        public bool IsMetric { get; set; }

    }
}
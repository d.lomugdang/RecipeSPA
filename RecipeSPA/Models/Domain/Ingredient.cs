﻿using RecipeSPA.Models.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RecipeSPA.Models
{
    public class Ingredient : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public double Amount { get; set; }
        [Required]
        public Measurement Measurement { get; set; }
    }

    public class IngredientComparer : IEqualityComparer<Ingredient>
    {
        public bool Equals(Ingredient x, Ingredient y)
        {
            if ((x == null || y == null) || (x.Id != y.Id))
                return false;

            return true;
        }

        public int GetHashCode(Ingredient obj)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using RecipeSPA.DAL;
using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using RecipeSPA.Models.ViewModels;

namespace RecipeSPA.Interfaces
{
    public interface IUnitOfWork
    {        
        Task<IEnumerable<Recipe>> GetAllRecipesByUser(RecipeUser user);
        Task<IEnumerable<Ingredient>> GetShoppingList(DateTime startDate, DateTime endDate);
        Recipe AddRecipe(Recipe newRecipe);
        Task<bool> SaveChangesAsync();
        void DeleteRecipe(int id);
        Recipe UpdateRecipe(RecipePatchModel recipe, int id);
        Task<IEnumerable<ScheduledRecipe>> GetScheduledRecipes(IEnumerable<int> recipeIds);
        Recipe GetRecipeById(int recipeId);
        ScheduledRecipe AddScheduledRecipe(ScheduledRecipe scheduledRecipe);
        void DeleteScheduleRecipe(int id);
        Task<IEnumerable<Conversion>> GetAllConversions();
        Task<IEnumerable<Ingredient>> GetAllIngredients();
        Task<IEnumerable<Measurement>> GetAllMeasurements();
        Ingredient AddIngredient(IngredientViewModel vm);
        void DeleteIngredient(int recipeId, int ingrId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeSPA.Interfaces
{
    public interface IRecipeValidator
    {
        bool IsDuplicateRecipeName(IEnumerable<string> nameCollection, string recipeName);
    }

    public class DuplicateRecipeNameException : Exception
    {
        public DuplicateRecipeNameException(string msg)
            : base(msg)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using RecipeSPA.Models.Domain;
using System.Threading.Tasks;
using RecipeSPA.Models;

namespace RecipeSPA.DAL
{
    public interface IBaseRepository<TEntity>  where TEntity : BaseEntity
    {
        TEntity Add(TEntity entity);
        bool Any(Expression<Func<TEntity, bool>> predicate);
        int Count();
        void Delete(TEntity entity);
        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> GetAll();
        TEntity GetById(int id);
        IQueryable<TEntity> GetQuery();
        Task<bool> SaveChanges();
        void Update(TEntity entity);
    }
}
﻿using RecipeSPA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RecipeSPA.Interfaces
{
    public interface IRecipeRepository
    {
        RecipeContext _context { get; }
        Task<Recipe> AddRecipe(Recipe recipe, string username);
        void AddIngredient(int id, string username, Ingredient newIngredient);
        void DeleteRecipeById(int id, string username);
        void DeleteScheduledRecipe(int scheduledRecipeId);
        bool UpdateRecipe(int id, Recipe recipe);
        IEnumerable<Recipe> GetAllRecipesByUsername(string username);
        IEnumerable<ScheduledRecipe> GetScheduledRecipesForUser(string name);
        IEnumerable<Conversion> GetConversions();
        IEnumerable<Measurement> GetMeasurements();
        Task<IEnumerable<string>> GetRecipeNames();
        Task<bool> SaveChangesAsync();
        Recipe GetRecipeById(int id);
        ScheduledRecipe AddScheduledRecipe(Recipe recipe, DateTime date);
        List<Ingredient> GetShoppingList(DateTime startDate, DateTime endDate);
    }
}

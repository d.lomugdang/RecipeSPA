﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RecipeSPA.Migrations
{
    public partial class AddTimestampToAll : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "ScheduledRecipes",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Recipes",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Measurements",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Ingredients",
                rowVersion: true,
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Conversions",
                rowVersion: true,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "ScheduledRecipes");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Recipes");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Measurements");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Ingredients");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Conversions");
        }
    }
}

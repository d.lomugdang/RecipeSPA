﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RecipeSPA.Migrations
{
    public partial class UpdateScheduledRecipeProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledRecipes_Recipes_RecipeId",
                table: "ScheduledRecipes");

            migrationBuilder.AlterColumn<int>(
                name: "RecipeId",
                table: "ScheduledRecipes",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledRecipes_Recipes_RecipeId",
                table: "ScheduledRecipes",
                column: "RecipeId",
                principalTable: "Recipes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduledRecipes_Recipes_RecipeId",
                table: "ScheduledRecipes");

            migrationBuilder.AlterColumn<int>(
                name: "RecipeId",
                table: "ScheduledRecipes",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduledRecipes_Recipes_RecipeId",
                table: "ScheduledRecipes",
                column: "RecipeId",
                principalTable: "Recipes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
